const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

let menu = [
    "1-DOLARES A PESOS",
    "2-PESOS A DOLARES",
    "3-PESOS A EUROS",
    "4-EUROS A PESOS",
];

console.log("Conversor de Moneda");

menu.forEach(function(elemento, indice) {
    console.log(elemento);
});

console.log("-------------------------");

rl.question("Elige una Opcion: ", (a) => {
    if (a <= 0) {
        console.log("Los numeros deben ser positivos y mayores a cero!");
        return rl.close();
    }
    if (isNaN(a)) {
        console.log("Debes escribir un numero!");
        return rl.close();
    }
    console.log("\n");

    var a = Number(a);
    let dolarPeso = 20.03; // 1 dolar en pesos mexicanos
    let PesoDolar = 0.05; //1 peso en dolares estadounidences
    let PesoEuro = 0.047; //1 peso en euros
    let EuroPeso = 21.28; //1 euro en pesos mexicanos

    switch (a) {
        case 1:
            console.log(menu[0]);
            rl.question("Ingresa la cantidad de dolares: ", (dolares) => {
                if (dolares <= 0) {
                    console.log("Los numeros deben ser positivos y mayores a cero!");
                    return rl.close();
                }
                if (isNaN(dolares)) {
                    console.log("Debes escribir un numero!");
                    return rl.close();
                }

                let r = Number(dolares) * dolarPeso;
                console.log(`La conversion de $ ${dolares} Dolares es: $ ${r} Pesos.`);
                console.log("\n");
                return rl.close();
            });
            break;
        case 2:
            console.log(menu[1]);
            rl.question("Ingresa la cantidad de pesos: ", (pesos) => {
                if (pesos <= 0) {
                    console.log("Los numeros deben ser positivos y mayores a cero!");
                    return rl.close();
                }
                if (isNaN(pesos)) {
                    console.log("Debes escribir un numero!");
                    return rl.close();
                }

                let r = Number(pesos) * PesoDolar;
                console.log(`La conversion de $ ${pesos} Pesos es: $ ${r} Dolares.`);
                console.log("\n");
                return rl.close();
            });
            break;
        case 3:
            console.log(menu[2]);
            rl.question("Ingresa la cantidad de pesos: ", (pesos) => {
                if (pesos <= 0) {
                    console.log("Los numeros deben ser positivos y mayores a cero!");
                    return rl.close();
                }
                if (isNaN(pesos)) {
                    console.log("Debes escribir un numero!");
                    return rl.close();
                }

                let r = Number(pesos) * PesoEuro;
                console.log(`La conversion de $ ${pesos} Pesos es: $ ${r} Euros.`);
                console.log("\n");
                return rl.close();
            });
            break;
        case 4:
            console.log(menu[3]);
            rl.question("Ingresa la cantidad de Euros: ", (euros) => {
                if (euros <= 0) {
                    console.log("Los numeros deben ser positivos y mayores a cero!");
                    return rl.close();
                }
                if (isNaN(euros)) {
                    console.log("Debes escribir un numero!");
                    return rl.close();
                }

                let r = (Number(euros) * EuroPeso).toFixed(2);
                console.log(`La conversion de € ${euros} Euros es: $ ${r} Pesos.`);
                console.log("\n");
                return rl.close();
            });
            break;
        default:
            console.log("Por favor, selecciona un valor del 1 al 4.");
    }
});